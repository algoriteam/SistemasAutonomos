﻿Sistemas Autónomos

Título:

    RobotSquad - Autonomous Systems + Clever, efficient and emotional robots in robocode platform + Cool maths for movement, shooting and targeting

Autores:

    A70377  André Filipe Proença e Silva
    A71184  João Pedro Pereira Fontes
    A64309  Pedro Vieira Fortes

Contato Email:
{a70377,a71184,a64309}@alunos.uminho.pt