package Comparators;

import Data.Measure;
import java.util.Comparator;

public class MeasureEfficiencyComparator implements Comparator<Measure>{

    @Override
    public int compare(Measure a, Measure b) {
        if(a.getEfficiency() > b.getEfficiency()) 
            return -1;
        else
            return 1;
    }
    
}
