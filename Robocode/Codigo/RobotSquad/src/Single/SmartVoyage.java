package Single;

import Data.ClasseSmart;
import Data.Robot;
import Data.Measure;

import robocode.*;

import java.util.ArrayList;
import java.util.Random;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.Iterator;
import java.util.LinkedHashMap;

/**
 * SmartVoyage
 * 
 * Robot similar to MultiPolar efficiency behaviour, except its movement. 
 * In this robot, there's no class control for its movement, it uses voronoi diagram's to control wich list of points he can move to (Always choose the closest point to its target)
 *
 * @author AlgoriTeam
 */
public class SmartVoyage extends AdvancedRobot {
    
    // ----- VARIABLES ----- //
    
        // BATTLEGROUND
        
            private double battleWidth;
            private double battleHeight;
        
        // STATUS
            
            private Boolean isAlive;
        
        // POSITION
            
            private Point2D position;
            private ArrayList<Point2D> safePositions;
            
            // ----- Parameters -----
            private Integer safeNetDensity = 3; // 5
            private Double safeNetConeAngle = 60.0; // 180.0
        
        // CLASS
            
            private ClasseSmart robotClass;
            
            private String currentShootingMethod;
            private String currentTargetingMethod;
        
        // MOVEMENT
            
            // ----- Parameters -----
            private Double speed = 30.0; // 30.0

            private Double wallSafeDistance = 50.0; // 50.0
            private Double wallTurningDegrees = 180.0; // 180.0
            private Double robotSafeDistance = this.wallSafeDistance + 2 * 40.0; // 100.0
        
        // DIRECTION + CONTROL
            
            private double directionAngle;
            private boolean directionSide;
        
        // HEADING
            
            private double bodyHeading;
            private double gunHeading;
        
        // TARGETING
            
            private String firstTarget;
            private String closestTarget;
            private String revengeTarget;
            private String slayerTarget;
            
            private Point2D shootingPoint;
            private LinkedHashMap<String, Robot> enemies;
            
            // ----- Parameters -----
            private Double radarDegrees = 360.0; // 10.0
            private Double linearDeviation = 30.0; // 100.0
            private Double circleDeviationDegrees = 360.0; // 360.0
            
            private Double startShootingDistance = 1000.0; // 200.0
        
        // POWER
            
            private Double power = 2.5; // 1.0
        
        // INFORMATION
            
            private Thread robotUpdater;
            private Thread targetUpdater;
            
            // ----- Parameters -----
            private Integer robotUpdateSpeed = 10; // 100
            private Integer targetUpdateSpeed = 10; // 100
        
        // VORONOI
            
            private Thread voronoiUpdater;
            private ArrayList<Point2D> voronoiPoints;
            private Point2D voronoyPoint;
            
            // ----- Parameters -----
            private Integer voronoiUpdateSpeed = 10; // 100
            private Integer voronoiSparsing = 10; // 1
            
        // DEBUG
        
            private Random randomPicker;
            
            // ----- Parameters -----
            private Boolean debugMode = true; // False
        
        
    // ----- RUN ----- //
    
    @Override
    public void run() {
        // Init values
        this.configureRobot();
        
        // Activate killing machine
        this.executeRobot();
    }
    
    // ----- EVENTS ----- //

    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        Robot oldEnemy;
        Robot newEnemy;
        
        String enemyName;
        Point2D enemyPosition;
        double enemyEnergy;
        double enemyHeading;
        double enemyVelocity;
        
        double angleToEnemy;
        double realAngle;
            
        // Calculate enemy positions
        synchronized(this.enemies) {
            // Add new position   
            angleToEnemy = e.getBearing();

            // Calculate the angle to the scanned robot
            // The ScannedRobotEvent gives us a bearing to the scanned robot but it is relative to our tank's position, not our radar's position. How do we resolve this little quandry?
            // We find our heading (getHeading()) and add the bearing to the scanned robot (e.getBearing())
            realAngle = Math.toRadians((this.getHeading() + angleToEnemy % 360));

            // Calculate enemy position
            enemyPosition = new Point2D.Double((this.getX() + Math.sin(realAngle) * e.getDistance()), 
                                               (this.getY() + Math.cos(realAngle) * e.getDistance()));
            
            enemyName = e.getName();
            enemyEnergy = e.getEnergy();
            enemyHeading = e.getHeading();
            enemyVelocity = e.getVelocity();
            newEnemy = new Robot(enemyPosition.getX(),
                                 enemyPosition.getY(),
                                 enemyHeading,
                                 angleToEnemy,
                                 enemyVelocity,
                                 enemyEnergy);
            
            // Check ifenemy info already exists
            if(!this.enemies.containsKey(enemyName))
                // Add recently found enemy 
                this.enemies.put(enemyName, newEnemy);
            else {
                // Update already found enemy 
                oldEnemy = this.enemies.get(e.getName());
                oldEnemy.setPosition(enemyPosition);
                oldEnemy.setBodyHeading(enemyHeading);
                oldEnemy.setVelocity(enemyVelocity);
                oldEnemy.setRobotEnergy(enemyEnergy);
            }
        }
    }

    @Override
    public void onRobotDeath(RobotDeathEvent e) {
        synchronized(this.enemies) {
            // Clear enemy history and restart scan
            this.enemies.clear();
        }
    }
    
    @Override
    public void onBulletMissed(BulletMissedEvent e){
        this.message("[DEBUG] Ohhh no, there goes a bullet ...");

        // Update classes
        this.robotClass.missShot();
        this.paintRobot(true);
    }

    @Override
    public void onBulletHit(BulletHitEvent e) {
        String enemyName = e.getName();
        this.message(String.format("[DEBUG] Muahahhaha, I shot '%s'!", enemyName));

        // Update classes
        this.robotClass.shootSomeone();
        this.paintRobot(true);
    }
    
    @Override
    public void onHitByBullet(HitByBulletEvent e) {
        synchronized(this.enemies) {
            Robot enemy;
            String enemyName = e.getName();
            double bulletPower = e.getBullet().getPower();
            double damage = bulletPower * 4;

            if(bulletPower > 1)
                damage += 2 * (bulletPower - 1);

            this.message(String.format("[DEBUG] AhhhHHHHH, '%s' shot me!", enemyName));

            // Is the enemy still alive????
            if(this.enemies.containsKey(enemyName)) {
                // Update damage received from enemy
                enemy = this.enemies.get(enemyName);
                enemy.makeDamage(damage);

                // Update classes
                this.robotClass.receiveShot();
                this.paintRobot(true);
            }
            else {
                this.message("[DEBUG] Howw??? A ghost shot me!");
            }
        }
    }
    
    @Override
    public void onRoundEnded(RoundEndedEvent e) {
        this.isAlive = false;
    }
    
    @Override
    public void onDeath(DeathEvent e) {
        this.isAlive = false;
    }
    
    @Override
    public void onWin(WinEvent e) {
        while(true) {
            // Victoryyyyyyyy celebration
            this.turnRight(360);
            
            // Shoot for fun
            this.fire(this.power);
        }
    }
    
    @Override
    public void onHitWall(HitWallEvent e) {
        // At best, this method is never called by the robot! But ifour safety hit check doesn't work, its here to help :)
        this.turnRight(this.wallTurningDegrees);
    }
    
    @Override
    public void onHitRobot(HitRobotEvent e) {
        // At best, this method is never called by the robot! But ifour safety hit check doesn't work, its here to help :)
        this.turnRight(this.wallTurningDegrees);
    }
    
    // ----- METHODS ----- //
    
    private void configureRobot() {
        this.message("> [CONFIGURATION]");
                
        // BATTLEGROUND
        this.battleWidth = this.getBattleFieldWidth();
        this.battleHeight = this.getBattleFieldHeight();
        this.message(String.format("  Width: %.0f", this.battleWidth));
        this.message(String.format("  Height: %.0f", this.battleHeight));
        
        // STATUS
        this.isAlive = true;
        
        // POSITION
        this.position = new Point2D.Double();
        this.position.setLocation(this.getX(), this.getY());
        this.message(String.format("  Position: (%.2f, %.2f)", this.position.getX(), this.position.getY()));
        
        this.safePositions = new ArrayList<>();
        
        if(this.safeNetDensity == null)
            this.safeNetDensity = 5;
        
        if(this.safeNetConeAngle == null)
            this.safeNetConeAngle = 180.0;
        
        // CLASS
        this.robotClass = new ClasseSmart();
        
        this.currentShootingMethod = "NONE";
        this.currentTargetingMethod = "NONE";
        
        this.message("  Class:");
        this.message(String.format("    Shooting: %s", this.robotClass.getBestShooting().getName()));
        this.message(String.format("    Targeting: %s", this.robotClass.getBestTargeting().getName()));
        
        // MOVEMENT
        if(this.speed == null)
            this.speed = 30.0;
        
        if(this.wallTurningDegrees == null)
            this.wallTurningDegrees = 180.0;

        if(this.wallSafeDistance == null)
            this.wallSafeDistance = 50.0;
        
        if(this.robotSafeDistance == null)
            this.robotSafeDistance = 100.0;
        
        this.message("  Movement:");
        this.message(String.format("    Speed: %.2f", this.speed));
        this.message(String.format("    Hit turning degrees: %.2f", this.wallTurningDegrees));
        this.message(String.format("    Wall safe distance: %.2f", this.wallSafeDistance));
        this.message(String.format("    Robot safe distance: %.2f", this.robotSafeDistance));
        
        // DIRECTION + CONTROL
        this.directionAngle = 0.0;
        this.directionSide = true;
        this.message("  Direction:");
        this.message(String.format("    Side: %s", this.directionSide ? "Right" : "Left"));
        this.message(String.format("    Angle: %.2f", this.directionAngle));
        
        // HEADING
        this.bodyHeading = this.getHeading();
        this.gunHeading = this.getGunHeading();
        this.message("  Heading:");
        this.message(String.format("    Body: %.2f", this.bodyHeading));
        this.message(String.format("    Gun: %.2f", this.gunHeading));
        
        // TARGETING
        this.firstTarget = "NONE";
        this.closestTarget = "NONE";
        this.revengeTarget = "NONE";
        this.slayerTarget = "NONE";
        this.shootingPoint = new Point2D.Double();
        this.enemies = new LinkedHashMap<>();
        
        if(this.radarDegrees == null)
            this.radarDegrees = 10.0;
        
        if(this.linearDeviation == null)
            this.linearDeviation = 100.0;
        
        if(this.circleDeviationDegrees == null)
            this.circleDeviationDegrees = 360.0;
        
        if (this.startShootingDistance == null)
            this.startShootingDistance = 200.0;
            
        this.message("  Radar:");
        this.message(String.format("    Turning degrees: %.2f", this.radarDegrees));
        this.message(String.format("    Linear deviation: %.2f", this.linearDeviation));
        this.message(String.format("    Circle deviation degrees: %.2f", this.circleDeviationDegrees));
        
        // POWER
        if(this.power == null)
            this.power = 1.0;
        this.message(String.format("  Power: %.0f", this.power));
        
        // INFORMATION
        this.robotUpdater = new Thread(new UpdateInfo());
        this.robotUpdater.start();
        
        this.targetUpdater = new Thread(new UpdateTarget());
        this.targetUpdater.start();
        
        this.voronoiUpdater = new Thread(new UpdateVoronoi());
        this.voronoiUpdater.start();
        
        this.voronoiPoints = new ArrayList<>();
        
        if(this.robotUpdateSpeed == null) {
            this.robotUpdateSpeed = 100;
        }
        
        if(this.targetUpdateSpeed == null) {
            this.targetUpdateSpeed = 100;
        }
        
        if(this.voronoiUpdateSpeed == null) {
            this.voronoiUpdateSpeed = 100;
        }
        
        if(this.voronoiSparsing == null) {
            this.voronoiSparsing = 1;
        }
        
        this.message("  Voronoi:");
        this.message(String.format("    Sparsing: %d", this.voronoiSparsing));
        
        // DEBUG
        this.randomPicker = new Random();
        
        // Other adjustments
        this.setAdjustRadarForRobotTurn(true);
        this.setAdjustRadarForGunTurn(true);
        this.setAdjustGunForRobotTurn(true);
        
        // Paint robot
        this.paintRobot(true);
    }
    
    private void executeRobot() {
        while (true) {
            // Scan
            this.setTurnRadarRight(this.radarDegrees);
            
            // Gun
            if(this.shootingPoint.getX() > 0 && this.shootingPoint.getY() > 0 && this.position.distance(this.shootingPoint) < this.startShootingDistance) {
            
                // Turn weapon to target
                this.turnGunToTarget(this.shootingPoint);

                // Fire weapon
                this.fire(this.power);
            
            }
                
            // Movement
            this.continueMovement();
            
            // Force movement
            this.execute();
            
            // Force scan
            this.scan();
            
            // Force paint
            this.paintRobot(false);
        }
    }
    
    // ----- AUXILIARY METHODS ----- //
    
    // MOVEMENT
    private void continueMovement() {
        synchronized(this.enemies) {
            if(this.enemies.size() > 0) {
                Point2D closestTank;;

                // Check ifrobot is going to hit a wall within the safe distance
                if(this.isGoingToHitWall()) {
                    this.turnBodyToTarget(new Point2D.Double(this.battleWidth * 0.5, this.battleHeight * 0.5), false);
                } else {
                    // Check ifrobot is going to hit someone within the safe distance
                    closestTank = this.isGoingToHitRobot();
                    if(closestTank != null) {
                        // Turn to target inverse (NOTE: Remember that this coordinate system only handles with positive values!)
                        this.turnBodyToTarget(new Point2D.Double(this.position.getX() - (closestTank.getX() - this.position.getX()), 
                                                                 this.position.getY() - (closestTank.getY() - this.position.getY())),
                                                                 false);
                    } else {
                        this.voronoyPoint = this.closestPointToTarget();
                        if(this.voronoyPoint != null)
                            this.turnBodyToTarget(this.voronoyPoint, true);
                    }
                }

                this.setAhead(this.speed);
            }
        }
    }
    
    private Point2D closestPointToTarget() {
        synchronized(this.voronoiPoints) {
            String targetName = this.getCurrentTarget();
            Point2D targetPosition;
            Point2D result = null;

            if(this.enemies.containsKey(targetName)) {
                targetPosition = this.enemies.get(targetName).getPosition();

                double minDistance = Double.MAX_VALUE;
                double currentMedDistance;

                for (Point2D p : this.voronoiPoints) {
                    currentMedDistance = this.position.distance(p) + targetPosition.distance(p);
                    if(currentMedDistance < minDistance) {
                        minDistance = currentMedDistance;
                        result = p;
                    }
                }
            }

            return result;
        }
    }
    
    private void turnBodyToTarget(Point2D t, boolean set) {
        double angle = this.normalRelativeAngle(this.absoluteBearing(this.position, t) - this.bodyHeading);
        if(set)
            this.setTurnRight(angle);
        else this.turnRight(angle);
    }
    
    private boolean isGoingToHitWall() {
        synchronized(this.safePositions){
            boolean result = false;
            
            for(Point2D pos: this.safePositions) {
                if(pos.getX() < 0.0 || pos.getX() > this.battleWidth || pos.getY() < 0.0 || pos.getY() > this.battleHeight) {
                    result = true;
                    break;
                }
            }
            
            return result;
        }
    }
    
    private Point2D isGoingToHitRobot() {
        Point2D result = null;
        Iterator<String> it = this.enemies.keySet().iterator();

        String currentName;
        Robot currentEnemy;
        Point2D currentPosition;
        double minDistance = Double.MAX_VALUE;
        double currentDistance;

        while(it.hasNext()) {
            currentName = it.next();
            currentEnemy = this.enemies.get(currentName);
            currentPosition = currentEnemy.getPosition();
            currentDistance = this.position.distance(currentPosition);
            if(currentDistance < (this.robotSafeDistance + this.speed) && (result == null || currentDistance < minDistance)) {
                minDistance = currentDistance;
                result = currentPosition;
            }
        }

        return result;
    }
    
    // TARGETING
    private String getCurrentTarget() {
        String result = "NONE";
        
        this.currentTargetingMethod = this.robotClass.getBestTargeting().getName();
        switch(this.currentTargetingMethod) {
            case "FIRST":
                result = this.firstTarget;
                break;
            case "CLOSEST":
                result = this.closestTarget;
                break;
            case "REVENGE":
                result = this.revengeTarget;
                break;
            case "SLAYER":
                result = this.slayerTarget;
                break;
        }
        
        return result;
    }
    
    private void turnGunToTarget(Point2D t) {
        double angle = this.normalRelativeAngle(this.absoluteBearing(this.position, t) - this.gunHeading);
        this.turnGunRight(angle);
    }
    
    // ----- CONSOLE METHODS ----- //
    
    private void message(String m) {
        System.out.println(m);
    }
    
    private String prettyArray(ArrayList<Measure> a) {
        StringBuilder sb = new StringBuilder();
        
        sb.append("[ ");
        for(Measure m : a){
            sb.append(String.format("%.2f (%s) ", m.getEfficiency(), m.getName()));
        }
        sb.append("]");
        
        return sb.toString();
    }
    
    // ----- DEBUG PAINT ----- //
    
    private void paintRobot(boolean debug) {
        Color color = Color.BLACK;
        String shooting = this.robotClass.getBestShooting().getName();
        String targeting = this.robotClass.getBestTargeting().getName();
        
        if(debug) {
            this.message("> [REPAINT]");

            // Class
            this.message("  Class:");
            this.message(String.format("    Shooting: %s -> %s", this.prettyArray(this.robotClass.getShootingMeasures()), shooting));
            this.message(String.format("    Targeting: %s -> %s", this.prettyArray(this.robotClass.getTargetingMeasures()), targeting));
        }
        
        // Movement
        this.setBodyColor(Color.BLACK);
        
        // Shooting
        switch(shooting) {
            case "POINT":
                color = Color.RED;
                break;
            case "LINEAR":
                color = Color.GREEN;
                break;
            case "CIRCLE":
                color = Color.BLUE;
                break;
            case "RANDOM":
                color = Color.YELLOW;
                break;
        }
        this.setGunColor(color);
        
        // Target
        switch(targeting) {
            case "FIRST":
                color = Color.RED;
                break;
            case "CLOSEST":
                color = Color.GREEN;
                break;
            case "REVENGE":
                color = Color.BLUE;
                break;
            case "SLAYER":
                color = Color.YELLOW;
                break;
        }
        this.setRadarColor(color);
    }
    
    @Override
    public void onPaint(Graphics2D g) {
        if (this.debugMode) {
            // Set the paint color to blue
            g.setColor(Color.BLUE);

            // Position
            g.fillOval((int) this.position.getX() - 8, (int) this.position.getY() - 8, 16, 16);

            // Set the paint color to red
            g.setColor(new Color(1.0f, 0, 0, 0.5f));
                        
            // Enemies
            synchronized(this.enemies) {
                Iterator<String> it = this.enemies.keySet().iterator();
               
                String enemyName;
                Robot enemy;
                Point2D enemyPosition;
                
                while(it.hasNext()) {
                    enemyName = it.next();
                    enemy = this.enemies.get(enemyName);
                    enemyPosition = enemy.getPosition();
                    g.fillRect((int) enemyPosition.getX() - 20, (int) enemyPosition.getY() - 20, 40, 40);
                }
            }
            
            // Set the paint color to pink
            g.setColor(Color.PINK);
            
            // Safe positions
            synchronized(this.safePositions) {
                for(Point2D pos : this.safePositions) {
                    g.fillOval((int) pos.getX() - 5, (int) pos.getY() - 5, 10, 10);
                }
            }
            
            // Set the paint color to white
            g.setColor(Color.WHITE);
            
            // Shooting point
            if (this.shootingPoint != null) {
                g.drawLine((int) this.position.getX(), (int) this.position.getY(), (int) this.shootingPoint.getX(), (int) this.shootingPoint.getY());
                g.fillOval((int) this.shootingPoint.getX() - 5, (int) this.shootingPoint.getY() - 5, 10, 10);
            }
            
            // VoronoiPoints
            synchronized(this.voronoiPoints) {
                for(Point2D p : this.voronoiPoints) {
                    g.fillOval((int) p.getX() - 2, (int) p.getY() - 2, 4, 4);
                }
            }
            
            // Set the paint color to yellow
            g.setColor(Color.YELLOW);
            
            // Moving point
            if (this.voronoyPoint != null)
                g.fillOval((int) voronoyPoint.getX() - 5, (int) voronoyPoint.getY() - 5, 10, 10);
        }
    }
    
    // ----- AUXILIARY METHODS ----- //
    
    private double absoluteBearing(Point2D source, Point2D target) {
        return Math.toDegrees(Math.atan2(target.getX() - source.getX(), target.getY() - source.getY()));
    }
    
    private double normalRelativeAngle(double angle) {
       double relativeAngle = angle % 360;
       if(relativeAngle <= -180)
           return 180 + (relativeAngle % 180);
       else if(relativeAngle > 180)
           return -180 + (relativeAngle % 180);
       else
           return relativeAngle;
    }
    
    // ----- INFORMATION THREAD ----- //
    
    // INFORMATION UPDATE
    private void updateRobot() {
        // Position
        this.position.setLocation(this.getX(), this.getY());
        
        // Heading
        this.bodyHeading = this.getHeading();
        this.gunHeading = this.getGunHeading();
        
        double correctAngle = -(this.bodyHeading - 90 - this.safeNetConeAngle * 0.5);
        
        double currentRadians;
        double jump = this.safeNetConeAngle / (this.safeNetDensity - 1);
        
        double vX;
        double vY;
        
        synchronized(this.safePositions) {
            // Clear list
            this.safePositions.clear();
            
            // Calculate new points
            for(int i = 0; i < this.safeNetDensity; i++){
                currentRadians = Math.toRadians(correctAngle - i * jump);
                vX = this.position.getX() + Math.cos(currentRadians) * (this.wallSafeDistance + this.speed);
                vY = this.position.getY() + Math.sin(currentRadians) * (this.wallSafeDistance + this.speed);

                this.safePositions.add(new Point2D.Double(vX, vY));
            }
        }
    }
    
    private void updateTargets() {
        Iterator<String> it = this.enemies.keySet().iterator();

        String currentName;
        Robot currentEnemy;

        // FIRST
        this.firstTarget = this.enemies.keySet().iterator().next();

        // CLOSEST
        String closestResult = "NONE";
        Point2D currentPosition;
        double minDistance = Double.MAX_VALUE;
        double currentDistance;

        // REVENGE
        String revengeResult = "NONE";
        double maxDamage = Double.MIN_VALUE;
        double currentDamage;

        // SLAYER
        String slayerResult = "NONE";
        double minLife = Double.MAX_VALUE;
        double currentLife;

        while(it.hasNext()) {
            currentName = it.next();
            currentEnemy = this.enemies.get(currentName);

            // CLOSEST
            currentPosition = currentEnemy.getPosition();
            currentDistance = this.position.distance(currentPosition);
            if(closestResult.equals("NONE") || currentDistance < minDistance) {
                minDistance = currentDistance;
                closestResult = currentName;
            }

            // REVENGE
            currentDamage = currentEnemy.getStolenEnergy();
            if(revengeResult.equals("NONE") || currentDamage > maxDamage) {
                maxDamage = currentDamage;
                revengeResult = currentName;
            }

            // SLAYER
            currentLife = currentEnemy.getRobotEnergy();
            if(slayerResult.equals("NONE") || currentLife < minLife) {
                minLife = currentLife;
                slayerResult = currentName;
            }
        }

        // Set new values
        this.closestTarget = closestResult;
        this.revengeTarget = revengeResult;
        this.slayerTarget = slayerResult;
    }
    
    private void updateShootingPoint() {
        String enemyName = this.getCurrentTarget();
        Robot enemy;
        Point2D enemyPosition;
        double enemyHeading;
        double enemyVelocity;

        Point2D tempShootingPoint = new Point2D.Double();

        // LINEAR AND CIRCLE
        double correctRadians;

        if(this.enemies.containsKey(enemyName)) {
            enemy = this.enemies.get(enemyName);
            enemyPosition = enemy.getPosition();
            enemyHeading = enemy.getBodyHeading();
            enemyVelocity = enemy.getVelocity() + this.linearDeviation;

            this.currentShootingMethod = this.robotClass.getBestShooting().getName();
            switch(this.currentShootingMethod){
                case "POINT":
                    tempShootingPoint = enemyPosition;
                    break;
                case "LINEAR":
                    correctRadians = Math.toRadians(-(enemyHeading - 90));
                    tempShootingPoint.setLocation(enemyPosition.getX() + Math.cos(correctRadians) * enemyVelocity,
                                                   enemyPosition.getY() + Math.sin(correctRadians) * enemyVelocity);
                    break;
                case "CIRCLE":
                    correctRadians = Math.toRadians(-(enemyHeading - 90) + this.randomPicker.nextDouble() * this.circleDeviationDegrees - this.circleDeviationDegrees * 0.5);
                    tempShootingPoint.setLocation(enemyPosition.getX() + Math.cos(correctRadians) * enemyVelocity,
                                                   enemyPosition.getY() + Math.sin(correctRadians) * enemyVelocity);
                    break;
                case "RANDOM":
                    tempShootingPoint.setLocation(enemyPosition.getX() + (2 * this.randomPicker.nextDouble() * enemyVelocity) - enemyVelocity, 
                                                   enemyPosition.getY() + (2 * this.randomPicker.nextDouble() * enemyVelocity) - enemyVelocity);
                    break;
            }

            this.shootingPoint = tempShootingPoint;
        }
    }
    
    private void updateVoronoi() {
        synchronized(this.voronoiPoints) {
            ArrayList<Point2D> positions = new ArrayList<>();
            Iterator<String> it = this.enemies.keySet().iterator();

            boolean firstEnemy = true;
            Point2D pixelPosition;
            Point2D oldClosest = new Point2D.Double(Double.MAX_VALUE, Double.MAX_VALUE);
            Point2D newClosest = new Point2D.Double();

            String currentName;
            Robot currentEnemy;
            Point2D currentPosition;

            // Add enemy positions + Self
            while(it.hasNext()) {
                currentName = it.next();
                currentEnemy = this.enemies.get(currentName);
                currentPosition = currentEnemy.getPosition();

                positions.add(new Point2D.Double(currentPosition.getX(), currentPosition.getY()));
            }
            positions.add(new Point2D.Double(this.position.getX(), this.position.getY()));

            // Clear old voronoi points
            this.voronoiPoints.clear();

            // Search for intersection points in the voronoi diagram
            for (int h = 0; h < this.battleHeight; h+=this.voronoiSparsing) {
                for (int w = 0; w < this.battleWidth; w+=this.voronoiSparsing) {
                    // Set pixel location
                    pixelPosition = new Point2D.Double(w, h);

                    // Check who is closer
                    for(Point2D p : positions) {
                        if(firstEnemy) {
                            newClosest.setLocation(p);
                            firstEnemy = false;
                        }

                        if(p.distance(pixelPosition) < newClosest.distance(pixelPosition)) {
                            newClosest.setLocation(p);
                        }
                    }

                    if(!oldClosest.equals(newClosest)){
                        oldClosest.setLocation(newClosest);
                        if(w != 0 && !this.voronoiPoints.contains(pixelPosition))
                            this.voronoiPoints.add(pixelPosition);
                    }
                }
            }
        }
    }
    
    private class UpdateInfo implements Runnable {
        
        @Override
        public void run() {
            while(true) {
                if(isAlive) {
                    // Update robot info
                    updateRobot();
                } 
                else break;
                
                try {
                    Thread.sleep(robotUpdateSpeed);
                } catch (InterruptedException ex) {
                    message("[DEBUG] Error in update thread sleep");
                }
            }
        }
        
    }
    
    private class UpdateTarget implements Runnable {
        
        @Override
        public void run() {
            while(true) {
                if(isAlive) {
                    synchronized(enemies) {
                        if(enemies.size() > 0) {
                            // Update target and shooting point info
                            updateTargets();
                            updateShootingPoint();
                        }
                    }
                } 
                else break;
                
                try {
                    Thread.sleep(targetUpdateSpeed);
                } catch (InterruptedException ex) {
                    message("[DEBUG] Error in update thread sleep");
                }
            }
        }
        
    }
    
    private class UpdateVoronoi implements Runnable {
        
        @Override
        public void run() {
            while(true) {
                if(isAlive) {
                    synchronized(enemies) {
                        if(enemies.size() > 0) {
                            // Update voronoi points
                            updateVoronoi();
                        }
                    }
                } 
                else break;
                
                try {
                    Thread.sleep(voronoiUpdateSpeed);
                } catch (InterruptedException ex) {
                    message("[DEBUG] Error in update thread sleep");
                }
            }
        }
        
    }
}
