package Examples;

import robocode.*;

import Tools.DistanceThread;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * VoyageRobot
 * 
 * Makes a clock-wise move around several obstacles
 *
 * @author AlgoriTeam
 */
public class VoyageRobot_Try extends Robot {
    
    // Distance
    private double best_distance;
    private double robot_distance;
    private DistanceThread counter;
    
    // Starting point
    private double start_x = 20.0;
    private double start_y = 20.0;
    private Point2D startingPoint;
    
    // Radar
    private Point2D last_robot;
    private int num_iteration;
    private Boolean canStart;
    
    // Obstacles
    private int num_obstacles;
    private HashMap<Integer, ArrayList<Point2D>> scanned_obstacles;
    private ArrayList<Point2D> obstacle_points;
    
    // Voyage
    private ArrayList<Point2D> calculated_route;
    private ArrayList<Point2D> safe_route;
    
    // Auxiliary
    private ArrayList<Integer> quadrants_right;
    private ArrayList<Integer> quadrants_left;
    private boolean debug_mode;
    
    private int safe_density = 4;
    private double safe_distance = 80.0;
    
    private Point2D safe_medium_point;
    private Point2D obstacles_medium_point;
    private double safe_min_distance;
    private double safe_medium_distance;
    private double safe_max_distance;
    
    private final double PI_H = 1.57079633;

    @Override
    public void onRoundEnded(RoundEndedEvent event) {
        counter.kill();
    }
   
    /**
     * VoyageRobot's run method
     */
    @Override
    public void run() {
        // Init values
        this.best_distance = 0.0;
        this.robot_distance = 0.0;
        this.counter = new DistanceThread(this);
        
        this.startingPoint = new Point2D.Double(start_x, start_y);
        
        this.last_robot = new Point2D.Double(0.0, 0.0);
        this.num_iteration = 0;
        this.canStart = false;
        
        this.num_obstacles = this.getOthers();
        this.scanned_obstacles = new HashMap<>();
        this.scanned_obstacles.put(this.num_iteration, new ArrayList<>());
        
        this.obstacle_points = new ArrayList<>();
        
        this.calculated_route = new ArrayList<>();
        this.safe_route = new ArrayList<>();
        
        this.quadrants_right = new ArrayList<>();
        this.quadrants_right.add(1);
        this.quadrants_right.add(2);
        this.quadrants_right.add(3);
        this.quadrants_right.add(4);
        
        this.quadrants_left = new ArrayList<>();
        this.quadrants_left.add(1);
        this.quadrants_left.add(4);
        this.quadrants_left.add(3);
        this.quadrants_left.add(2);
        
        this.debug_mode = false;
        
        // Scan for obstacles
        scanObstacles();
    }

    /**
     * Fire when we see a robot
     * 
     * @param e
     */
    @Override
    public void onScannedRobot(ScannedRobotEvent e) {
        // Scanned a new Robot     
        double angleToEnemy = e.getBearing();

        // Calculate the angle to the scanned robot
        // The ScannedRobotEvent gives us a bearing to the scanned robot but it is relative to our tank's position, not our radar's position. How do we resolve this little quandry?
        // We find our heading (getHeading()) and add the bearing to the scanned robot (e.getBearing())
        double angle = Math.toRadians((this.getHeading() + angleToEnemy % 360));

        // Calculate the coordinates of the robot
        double x = (this.getX() + Math.sin(angle) * e.getDistance());
        double y = (this.getY() + Math.cos(angle) * e.getDistance());
        
        Point2D pos = new Point2D.Double(x, y);
        
        // System.out.println("[DEBUG] Found obstacle in point (" + x + "," + y + ") [" + this.num_iteration + "]");
        if (!this.canStart) foundObstacle(pos);
    }
    
    public void scanObstacles() {     
        // Find obstacle coordinates
        System.out.println("[SCAN] Scanning for obstacles");
        while(!this.canStart) {
            continueScan();
        }
        System.out.println("[SCAN] All obstacles are in place now");
        moveToStart();
    }
    
    public void moveToStart() {
        DecimalFormat df = new DecimalFormat("#.00");
        
        // Robot current location
        Point2D pos = new Point2D.Double(this.getX(), this.getY());
        Point2D closest = closestPoint(pos, this.obstacle_points);
        int robot_quad = pointQuadrant(pos);
        int closest_quad = pointQuadrant(closest);
        int start_quad = pointQuadrant(this.startingPoint);
        
        System.out.println("[START] Robot current location is (" + df.format(pos.getX()) + " | " + df.format(pos.getY()) + ")");
        System.out.println("[START] Robot current quadrant is " + robot_quad);
        System.out.println("[START] Closest obstacle current quadrant is " + closest_quad);
        
        // Calculate safe trajectory
        ArrayList<Point2D> safe = new ArrayList<>();
        
        if (robot_quad == start_quad) {
            // Robot is in the same quadrant as the starting point and the closest obstacle
            if (robot_quad == closest_quad) {
                // Starting point is closer or closest obstacle is "above" robot position
                if (pos.distance(this.startingPoint) < pos.distance(closest) || pointPosition(closest, pos) != 3) {
                    safe.add(this.startingPoint);
                }
                else {
                    // Quadrants 1 -> 2 -> S -> 4
                    safe.addAll(quadrantSequence(robot_quad, start_quad));
                }
            }
            else {
                safe.add(this.startingPoint);
            }
        } else {
            int closest_direction = this.pointPosition(closest, pos);
            System.out.println("[START] Closest obstacle current direction is " + closest_direction);
            
            Point2D closest_wall = closestSafeWall(pos, closest_direction);
            int wall_quad = pointQuadrant(closest_wall);
            
            System.out.println("[START] Closest wall quadrant is " + wall_quad);
            
            // Add closest wall point
            safe.add(closest_wall);
            
            // Add quadrant sequence (1 -> 2 -> 3 -> 4)
            safe.addAll(quadrantSequence(wall_quad, start_quad));
        }
            
        // Go to starting point (20, 20)
        System.out.println("[START] Going to the starting point");
        moveToList(safe);
        System.out.println("[START] Arrived to the starting point");
        
        // Corrent tank angle
        double facing = this.getHeading();
        double correct = 45 - facing;
        turnRight(correct);
        
        // Start voyage
        scanObstaclesAgain();
    }
    
    public void scanObstaclesAgain() {
        this.canStart = false;
        
        // Find obstacle coordinates
        System.out.println("[SCAN] Scanning for obstacles");
        while(!this.canStart) {
            continueScan();
        }
        System.out.println("[SCAN] All obstacles are in place now");
        makeVoyage();
    }
    
    public void makeVoyage() {
        DecimalFormat df = new DecimalFormat("#.00");
        System.out.println("[VOYAGE] Unordered-points ------------------------------------ ");
        for (Point2D p : this.obstacle_points) {
            System.out.println("POINT: ("+ df.format(p.getX()) + " | " + df.format(p.getY()) +")");
        }
        
        // Order obstacle-list clock-wise
        this.obstacles_medium_point = mediumPoint(this.obstacle_points);
        orderPoints(this.obstacle_points, this.startingPoint, this.obstacles_medium_point);
        System.out.println("[VOYAGE] Ordered-points -------------------------------------- ");
        for (Point2D p : this.obstacle_points) {
            System.out.println("POINT: ("+ df.format(p.getX()) + " | " + df.format(p.getY()) +")");
        }
        
        // Search for shortest path
        this.calculated_route = shortestPath(this.startingPoint, this.obstacle_points);
        System.out.println("[VOYAGE] Shortest-path --------------------------------------- ");
        for (Point2D p : this.calculated_route) {
            System.out.println("POINT: ("+ df.format(p.getX()) + " | " + df.format(p.getY()) +")");
        }
        
        // Search for safest path
        // Calculate mediumDistance to center point
        this.safe_medium_point = mediumPoint(this.obstacle_points);
        this.safe_medium_distance = mediumDistance(this.obstacle_points, this.safe_medium_point);
        this.safe_route = safePath(this.calculated_route, this.safe_medium_point, this.safe_medium_distance);
        
        // Order safepath-list clock-wise
        orderPoints(this.safe_route, this.startingPoint, this.safe_medium_point);
        
        // DEBUG - Ordering is disappearing with last position
        this.safe_route.add(this.startingPoint);
        
        System.out.println("[VOYAGE] Safest-path --------------------------------------- ");
        for (Point2D p : this.safe_route) {
            System.out.println("POINT: ("+ df.format(p.getX()) + " | " + df.format(p.getY()) +")");
        }
        
        this.debug_mode = true;
        
        // Calculate best_distance
        this.best_distance = calculateDistance(this.calculated_route);
        
        System.out.println("[VOYAGE] Best predictable distance is " + df.format(this.best_distance) + " pixels");
        
        // Make voyage
        this.counter.start();
        System.out.println("[VOYAGE] Starting voyage");
        moveToList(this.safe_route);
        System.out.println("[VOYAGE] Ending voyage");
        
        // Get robot distance
        this.robot_distance = this.counter.getDistance();
        this.counter.kill();
        
        System.out.println("[RESULT] Robot walked a distance of " + df.format(this.robot_distance) + " pixels");
        System.out.println("[RESULT] We found a level of efficiency of " + df.format((this.best_distance/this.robot_distance) * 100) + " %");
    }
    
    // ----- EXTRA METHODS ----- //
    
    // ----- SACAN METHODS ----- // 
    
    private void continueScan() {
        this.turnRadarRight(10);
    }
    
    private void foundObstacle(Point2D p) {
        // Check if any obstacles were found during this iteration
        if (this.scanned_obstacles.containsKey(this.num_iteration)) {
            ArrayList<Point2D> reference = this.scanned_obstacles.get(this.num_iteration);
            int num_current_scanned_obstacles = reference.size();
            
            if(num_current_scanned_obstacles == this.num_obstacles) {
                // Compare the 2 older lists to check if they are equal, that is, the obstacles didn't move from place
                if (this.num_iteration > 0) {
                    ArrayList<Point2D> oldder = this.scanned_obstacles.get(this.num_iteration - 1);
                    boolean equal = equalList(reference, oldder);
                    
                    System.out.println("[SCAN] Stopped: " + equal);
                    
                    if (equal) {
                        this.obstacle_points = new ArrayList<>(reference);
                        this.canStart = true;
                        
                        // Clear objects
                        this.num_iteration = 0;
                        this.scanned_obstacles = new HashMap<>();
                        this.scanned_obstacles.put(this.num_iteration, new ArrayList<>());
                    }
                }

                // Create new list for new iteration
                this.num_iteration++;
                
                ArrayList<Point2D> new_list = new ArrayList<>();
                new_list.add(p);
                
                this.scanned_obstacles.put(this.num_iteration, new_list);
            }
            else {
                // Add new obstacle point
                if (p.distance(this.last_robot) >= 50.0) {
                    reference.add(p);
                }
                
                this.last_robot = p;
            }
        }
        else {
            System.out.println("[DEBUG] Obstacle list from new radar iteration wasn't created properly in the previous turn ...");
        }
    }
    
    private boolean equalList(ArrayList<Point2D> a, ArrayList<Point2D> b) {
        boolean res = true;
        
        for (Point2D p : a) {
            if (!b.contains(p)){
                res = false;
                break;
            }
        }
        
        return res;
    }
    
    // ----- MOVE METHODS ----- //
    
    private void moveToList(ArrayList<Point2D> a) {
        int size = a.size();
        int current = 0;
        
        Point2D target;
        boolean isthere = false;
        
        while(current < size) {
            target = a.get(current);
            isthere = moveTo(target);
            
            if (isthere) current++;
        }
    }
    
    private boolean moveTo(Point2D target) {
        Point2D.Double location = new Point2D.Double(getX(), getY());

        if (location.equals(target)) {
            return true;
        }
        else {
            double distance = location.distance(target);
            double angle = normalRelativeAngle(absoluteBearing(location, target) - getHeading());
            if (Math.abs(angle) > 90.0) {
                distance *= -1.0;
                if (angle > 0.0) {
                    angle -= 180.0;
                }
                else {
                    angle += 180.0;
                }
            }
            turnRight(angle);
            ahead(distance);
            
            return false;
        }
   }
    
    private double absoluteBearing(Point2D source, Point2D target) {
        return Math.toDegrees(Math.atan2(target.getX() - source.getX(), target.getY() - source.getY()));
    }
    
    private double normalRelativeAngle(double angle) {
       double relativeAngle = angle % 360;
       if (relativeAngle <= -180)
           return 180 + (relativeAngle % 180);
       else if (relativeAngle > 180)
           return -180 + (relativeAngle % 180);
       else
           return relativeAngle;
    }
    
    // ----- POINT METHODS ----- //
    
    private int pointPosition(Point2D a, Point2D b) {
        int res;
        
        if (a.getX() > b.getX() && a.getY() > b.getY()) {
            res = 1;
        } else {
            if (a.getX() > b.getX() && a.getY() < b.getY()) {
                res = 2;
            } else {
                if (a.getX() < b.getX() && a.getY() < b.getY()) {
                    res = 3;
                } else {
                    res = 4;
                }
            }
        }
        return res;
    }
    
    private Point2D closestPoint(Point2D a, ArrayList<Point2D> b) {
        Point2D closest = null;
        double min_distance = Float.MAX_VALUE;
        double distance;
        
        for (Point2D p : b) {
            distance = a.distance(p);
            if (distance < min_distance) {
                min_distance = distance;
                closest = p;
            }
        }
        
        return closest;
    }
    
    private Point2D mediumPoint(ArrayList<Point2D> a) {
        double med_x = 0.0;
        double med_y = 0.0;
        
        int size = a.size();
        
        for (Point2D p : a) {
            med_x += p.getX();
            med_y += p.getY();
        }
        
        med_x = med_x / size;
        med_y = med_y / size;
        
        return new Point2D.Double(med_x, med_y);
    }
    
    private double minDistance(ArrayList<Point2D> a, Point2D m) {
        double min_distance = Float.MAX_VALUE;
        
        double distance;
        for (Point2D p : a) {
            distance = p.distance(m);
            if (distance < min_distance)
                min_distance = distance;
        }
        
        return min_distance;
    }
    
    private double mediumDistance(ArrayList<Point2D> a, Point2D m) {
        double res = 0.0;
        
        for (Point2D p : a) {
            res += p.distance(m);
        }
        
        res = res / a.size();
        return res;
    }
    
    private double maxDistance(ArrayList<Point2D> a, Point2D m) {
        double max_distance = 0.0;
        
        double distance;
        for (Point2D p : a) {
            distance = p.distance(m);
            if (distance > max_distance)
                max_distance = distance;
        }
        
        return max_distance;
    }
    
    private int pointQuadrant(Point2D p) {
        int quadrant;
        
        double x = p.getX();
        double y = p.getY();
        
        double mid_x = this.getBattleFieldWidth() * 0.5;
        double mid_y = this.getBattleFieldHeight() * 0.5;
        
        if (x >= mid_x && y >= mid_y) {
            quadrant = 1;
        } else {
            if (x > mid_x && y < mid_y) {
                quadrant = 2;
            } else {
                if (x < mid_x && y < mid_y) {
                    quadrant = 3;
                } else {
                    quadrant = 4;
                }
            }
        }
       
        return quadrant;
    }
    
    private Point2D quadrantPoint(int i) {
        Point2D res;
        
        switch(i) {
            case 1:
                res = new Point2D.Double(this.getBattleFieldWidth() - 20, this.getBattleFieldHeight() - 20);
                break;
            case 2:
                res = new Point2D.Double(this.getBattleFieldWidth() - 20, 20);
                break;
            case 3:
                res = new Point2D.Double(20, 20);
                break;
            case 4:
                res = new Point2D.Double(20, this.getBattleFieldHeight() - 20);
                break;
            default:
                res = null;
                break;
        }
       
        return res;
    }
    
    private Point2D negatePoint(Point2D p) {
        Point2D res;
        
        double x = p.getX();
        double y = p.getY();
        
        double mid_x = this.getBattleFieldWidth() * 0.5;
        double mid_y = this.getBattleFieldHeight() * 0.5;
        
        if (x >= mid_x && y >= mid_y) {
            // Quadrant 1;
            res = new Point2D.Double(x - mid_x, y - mid_y);
        } else {
            if (x > mid_x && y < mid_y) {
                // Quadrant 2;
                res = new Point2D.Double(x - mid_x, y + mid_y);
            } else {
                if (x < mid_x && y < mid_y) {
                    // Quadrant 3;
                    res = new Point2D.Double(x + mid_x, y + mid_y);
                } else {
                    // Quadrant 4;
                    res = new Point2D.Double(x + mid_x, y - mid_y);
                }
            }
        }
        
        return res;
    }
    
    private void orderPoints(ArrayList<Point2D> a, Point2D start, Point2D center) {
        Collections.sort(a, (Point2D o1, Point2D o2) -> {
            double angle_o1 = angleBetweenLines(center, start, center, o1);
            double angle_o2 = angleBetweenLines(center, start, center, o2);
            
            if (angle_o1 < angle_o2) {
                return -1;
            } else {
                return 1;
            }
        });
    }
    
    private double angleBetweenLines(Point2D a1, Point2D a2, Point2D b1, Point2D b2) {
        double angle1 = Math.atan2(a1.getY() - a2.getY(), a1.getX() - a2.getX());
        double angle2 = Math.atan2(b1.getY() - b2.getY(), b1.getX() - b2.getX());
        double calculatedAngle = Math.toDegrees(angle1 - angle2);
        
        if (calculatedAngle < 0) 
            calculatedAngle += 360;
        
        return calculatedAngle;
    }
    
    // ----- QUADRANT SAFE POINTS ----- //
    
    private int rightCount(int source, int target) {
        int res = 0;
        int index = this.quadrants_right.indexOf(source);
        int value = this.quadrants_right.get(index);
        
        while (value != target) {
            index = (index + 1) % 4;
            value = this.quadrants_right.get(index);
            
            res++;
        }
        
        return res;
    }
    
    private int leftCount(int source, int target) {
        int res = 0;
        int index = this.quadrants_left.indexOf(source);
        int value = this.quadrants_left.get(index);
        
        while (value != target) {
            index = (index + 1) % 4;
            value = this.quadrants_left.get(index);
            
            res++;
        }
        
        return res;
    }
    
    private ArrayList<Integer> minimumSequence(int source, int target) {
        if (rightCount(source, target) <= leftCount(source, target)) {
            return this.quadrants_right;
        }
        else return this.quadrants_left;   
    }
    
    private ArrayList<Point2D> quadrantSequence(int source, int target) {
        ArrayList<Point2D> res = new ArrayList<>();
        ArrayList<Integer> minimum_route = minimumSequence(source, target);
        int index = minimum_route.indexOf(source);
        int value = minimum_route.get(index);
        
        Point2D p;
        p = this.quadrantPoint(value);
        res.add(p);
        
        while (value != target) {
            index = (index + 1) % 4;
            value = minimum_route.get(index);
            
            p = this.quadrantPoint(value);
            res.add(p);
        }
        
        return res;
    }
    
    // ----- CLOSEST WALLS ----- //
    
    private Point2D wall(Point2D pos, int d) {
        Point2D res;
        
        switch(d) {
            // Right
            case 0:
                res = new Point2D.Double(this.getBattleFieldWidth() - 20, pos.getY());
                break;
            // Up
            case 1:
                res = new Point2D.Double(pos.getX(), this.getBattleFieldHeight() - 20);
                break;
            // Left
            case 2:
                res = new Point2D.Double(20, pos.getY());
                break;
            // Down
            case 3:
                res = new Point2D.Double(pos.getX(), 20);
                break;
            default:
                res = null;
                break;
        }
        
        return res;
    }
    
    private ArrayList<Point2D> safeWalls(Point2D a, int obs) {
        // 1 -> Down, Left
        // 2 -> Up, Left
        // 3 -> Up, Right
        // 4 -> Down, Right
        ArrayList<Point2D> res = new ArrayList<>();
        
        switch(obs) {
            case 1:
                res.add(wall(a,3));
                res.add(wall(a,2));
                break;
            case 2:
                res.add(wall(a,1));
                res.add(wall(a,2));
                break;
            case 3:
                res.add(wall(a,1));
                res.add(wall(a,0));
                break;
            case 4:
                res.add(wall(a,3));
                res.add(wall(a,0));
                break;
        }
        
        return res;
    }
    
    private Point2D closestSafeWall(Point2D a, int obs) {
        ArrayList<Point2D> walls = safeWalls(a, obs);
        Point2D closest_wall = closestPoint(a, walls);
        
        return closest_wall;
    }
    
    // ----- SHORTEST PATH ----- //
    
    private double calculateDistance(ArrayList<Point2D> p) {
        int size = p.size();
        double distance = 0.0;
        
        Point2D a, b;
        for (int i = 1; i < size; i++){
            a = p.get(i-1);
            b = p.get(i);
            distance += a.distance(b);
        }
        
        return distance;
    }
            
    private ArrayList<Point2D> shortestPath(Point2D start, ArrayList<Point2D> path) {
        ArrayList<Point2D> visited = new ArrayList<>();
        ArrayList<Point2D> unvisited = new ArrayList<>(path);

        Point2D first = path.get(0);
        Point2D current_point;
        
        Point2D closest_point = null;
        double distance;
        double min_distance;
        boolean done = false;
        
        // Add start point (Real starting point) and first node
        visited.add(start);
        visited.add(first);
        
        // Remove first point from path
        unvisited.remove(first);
        
        while (!done) {     
            min_distance = Float.MAX_VALUE;
            current_point = visited.get(visited.size() - 1);
            
            // Calculate closest point
            for (Point2D nv : unvisited) {
                distance = current_point.distance(nv);
                if (distance <= min_distance) {
                    min_distance = distance;
                    closest_point = nv;
                }
            }
            
            // Add closest point to VISITED and remove it from UNVISITED
            unvisited.remove(closest_point);
            visited.add(closest_point);

            // No more points left
            if (unvisited.size() <= 0) done = true;
        }
        
        // And ending point
        visited.add(start);
        return visited;
    }
    
    private ArrayList<Point2D> safePath(ArrayList<Point2D> path, Point2D med, double distance) {
        ArrayList<Point2D> res = new ArrayList<>();
        Point2D start = path.get(0);
        
        ArrayList<Point2D> circular;
        Point2D current;
                
        // Add starting point
        res.add(start);
        
        // Calculate extra circun-tanky points (Prevents hit) 
        for(int i = 1; i + 1 < path.size(); i++){
            current = path.get(i);
            circular = circularPoints(current, this.safe_distance, this.safe_density);
            res.addAll(circular);
        }
        
        // Clean extra points
        cleanPoints(res, med, distance);
                
        return res;
    }
    
    // ----- AVOIDING POINTS ----- //
    
    private ArrayList<Point2D> circularPoints(Point2D p, double radius, int density) {
        ArrayList<Point2D> res = new ArrayList<>();
        double x, y;
        double jump_angle = PI_H / density;
        int number_jumps = 4 * density;
        
        for (int i = 0; i < number_jumps; i++) {
            x = p.getX() + radius * Math.cos(i * jump_angle);
            y = p.getY() + radius * Math.sin(i * jump_angle);
            res.add(new Point2D.Double(x, y));
        }
        
        return res;
    }
    
    private void cleanPoints(ArrayList<Point2D> a, Point2D medium, double distance) {
        ArrayList<Point2D> remove = new ArrayList<>();
        double d;
        
        for (Point2D p : a) {
            d = p.distance(medium);
            
            // If point is inside the circle, remove it
            if (d < distance)
                remove.add(p);
        }
        
        a.removeAll(remove);
    }
    
    // ----- DEBUG PAINT ----- //
    
    @Override
    public void onPaint(Graphics2D g) {
     if (this.debug_mode){
        // Set the paint color to red
        g.setColor(Color.RED);
        
        for (Point2D p : this.calculated_route) {
            // Paint a filled rectangle at (50,50) at size 100x150 pixels
            g.fillOval((int) p.getX(), (int) p.getY(), 5, 5);
        }
        
        // Set the paint color to green
        g.setColor(Color.GREEN);
        
        for (Point2D p : this.safe_route) {
            // Paint a filled rectangle at (50,50) at size 100x150 pixels
            g.fillOval((int) p.getX(), (int) p.getY(), 5, 5);
        }
        
        // Set the paint color to blue
        g.setColor(new Color(0.0f, 0.0f, 1.0f, 0.3f));
        g.fillOval((int) (this.safe_medium_point.getX() - this.safe_max_distance), (int) (this.safe_medium_point.getY() - this.safe_max_distance), (int) this.safe_max_distance * 2, (int) this.safe_max_distance * 2);
        
        g.setColor(new Color(0.0f, 0.0f, 1.0f, 0.3f));
        g.fillOval((int) (this.safe_medium_point.getX() - this.safe_min_distance), (int) (this.safe_medium_point.getY() - this.safe_min_distance), (int) this.safe_min_distance * 2, (int) this.safe_min_distance * 2);
        
        // Set the paint color to blue
        g.fillOval((int) (this.safe_medium_point.getX() - this.safe_medium_distance), (int) (this.safe_medium_point.getY() - this.safe_medium_distance), (int) this.safe_medium_distance * 2, (int) this.safe_medium_distance * 2);
        
        // Set the paint color to yellow
        g.setColor(Color.YELLOW);
        g.fillOval((int) this.obstacles_medium_point.getX(), (int) this.obstacles_medium_point.getY(), 5, 5);
        
        // Set the paint color to white
        g.setColor(Color.WHITE);
        g.fillOval((int) this.safe_medium_point.getX(), (int) this.safe_medium_point.getY(), 5, 5);
     }
 }
}												
