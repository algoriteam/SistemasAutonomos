package Data;

public class Measure {
    
    // ----- VARIABLES ----- //
    
        private String name;

        private int type;
        private double valueMissedShots;
        private double valueTimesShot;
        private double valueSuccessfullHits;
    
    // ----- CONSTRUCTORS ----- //
    
    public Measure(String n, int t) {
        this.name = n;
        this.type = t;
        this.valueMissedShots = 0.0;
        this.valueTimesShot = 0.0;
        this.valueSuccessfullHits = 0.0;
    }
    
    public Measure(String n, int t, double nt, double ms, double ns) {
        this.name = n;
        this.type = t;
        this.valueTimesShot = ms;
        this.valueTimesShot = nt;
        this.valueSuccessfullHits = ns;
    }
    
    public Measure(Measure m) {
        this.name = m.getName();
        this.type = m.getType();
        this.valueTimesShot = m.getValueMissedShots();
        this.valueTimesShot = m.getValueTimesShot();
        this.valueSuccessfullHits = m.getValueSuccessfullHits();
    }
    
    // ----- GETS ----- //
    
    public String getName() {
        return this.name;
    }

    public int getType() {
        return this.type;
    }

    public double getValueMissedShots() {
        return this.valueMissedShots;
    }

    public double getValueTimesShot() {
        return this.valueTimesShot;
    }

    public double getValueSuccessfullHits() {
        return this.valueSuccessfullHits;
    }
    
    // ----- SETS ----- //
    
    public void setName(String n) {
        this.name = n;
    }

    public void setType(int t) {
        this.type = t;
    }

    public void setValueMissedShots(double n) {
        this.valueMissedShots = n;
    }

    public void setValueTimesShots(double n) {
        this.valueTimesShot = n;
    }

    public void setValueSuccessfullHits(double n) {
        this.valueSuccessfullHits = n;
    }
    
    // ----- CLONE + EQUALS ---- //
    
    @Override
    public Measure clone(){
        return new Measure(this);
    }
    
    @Override
    public boolean equals(Object o) {
        boolean result;
        
        // Check class
        if((o == null) || (this.getClass() != o.getClass())) {
            result = false;
        } 
        // Check position
        else {
            Measure measure = (Measure) o;
            result = this.name.equals(measure.getName()) && 
                     this.type == measure.getType() &&
                     this.valueMissedShots == measure.getValueMissedShots() &&
                     this.valueTimesShot == measure.getValueTimesShot() && 
                     this.valueSuccessfullHits == measure.getValueSuccessfullHits();
        }

        return result;
    }
    
    // ----- METHODS ----- //
    
    public void missShot() {
        double value = 0.0;
        
        // Watch out for this value! Use small values because remember, a robot can miss a lot ...
        switch(type) {
            // MOVEMENT
            case 0:
                value = 0.1;
                break;
            // SHOOTING
            case 1:
                value = 0.3;
                break;
            // TARGETING
            case 2:
                value = 0.2;
                break;
            // EMOTION
            case 3:
                value = 0.5;
                break;
        }
        
        this.valueMissedShots += value;
    }
    
    public void receiveShot() {
        double value = 0.0;
        
        switch(type) {
            // MOVEMENT
            case 0:
                value = 0.7;
                break;
            // SHOOTING
            case 1:
                value = 0.0;
                break;
            // TARGETING
            case 2:
                value = 0.4;
                break;
            // EMOTION
            case 3:
                value = 0.8;
                break;
        }
        
        this.valueTimesShot += value;
    }
    
    public void shootSomeone() {
        double value = 0.0;
        
        switch(type) {
            // MOVEMENT
            case 0:
                value = 1.0;
                break;
            // SHOOTING
            case 1:
                value = 1.1;
                break;
            // TARGETING
            case 2:
                value = 1.0;
                break;
            // EMOTION
            case 3:
                value = 1.0;
                break;
        }
        
        this.valueSuccessfullHits += value;
    }
    
    public double getEfficiency() {
        return (double)(this.valueSuccessfullHits + 1.0) / (double)(this.valueSuccessfullHits + this.valueTimesShot + this.valueMissedShots + 1.0);
    }
    
}
