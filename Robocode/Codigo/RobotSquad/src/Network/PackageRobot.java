package Network;

import Data.Robot;
import java.io.Serializable;
import java.util.LinkedHashMap;

public class PackageRobot implements Serializable {
    
    // ----- VARIABLES ----- //
        
        // Code
        private int code;
        
        // Object
        private Object object;
            
    // ----- CONSTRUCTORS ----- //
        
    public PackageRobot(int c, Object o) {
        this.code = c;
        this.object = o;
    }
    
    public PackageRobot(PackageRobot p) {
        this.code = p.getCode();
        this.object = p.getObject();
    }
    
    // ----- GETS ----- //
    
    public int getCode() {
        return this.code;
    }

    public Object getObject() {
        return this.object;
    }
    
    // ----- SETS ----- //

    public void setCode(int c) {
        this.code = c;
    }

    public void setObject(Object o) {
        this.object = o;
    }
    
    // ----- CLONE + EQUALS ---- //
    
    @Override
    public PackageRobot clone(){
        return new PackageRobot(this);
    }
    
    @Override
    public boolean equals(Object o) {
        boolean result;
        
        // Check class
        if((o == null) || (!this.getClass().equals(o.getClass()))) {
            result = false;
        } 
        // Check position
        else {
            PackageRobot pack = (PackageRobot) o;
            result = this.code == pack.getCode() &&
                     this.object.equals(pack.getObject());
        }

        return result;
    }
        
}
