import math, time
import datetime

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from keras.utils import plot_model
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.layers.recurrent import LSTM

class ADVERTISING:

    # ----- VARIABLES -----

    # Fixar random seed para se puder reproduzir os resultados
    seed = 9
    np.random.seed(seed)

    # Dados
    dataset_filename = './resources/advertising-and-sales-data-36-co.csv'
    training_percentage = 2/3
    sliding_window = 2

    # Rede
    batch_size = 10
    epochs = 350

    # Etapa 1 - Carregar o dataset
    def get_advertising_dataset(self, file_name):
        col_names = ['Month','Avertising','Sales']

        # Fica numa especie de tabela exactamente como estava no csv
        sales = pd.read_csv(file_name, header=0, names=col_names)
        df = pd.DataFrame(sales)

        # Vou só ficar com as colunas 2,3, visto que a primeira coluna corresponde ao número do mês e o dataset já se encontra ordenado
        df.drop(df.columns[0], axis=1, inplace=True)
        return df

    # Etapa 2 - Visualizar o dataset
    def visualize_advertising_dataset(self, filename):
    	df = self.get_advertising_dataset(filename)
    	print(df.head())

    # Etapa 3 - Preparar o dataset
    '''
        Função load_data do lstm.py configurada para aceitar qualquer número de parametros
        O último atributo é que fica como label (resultado)
        Stock é um dataframe do pandas (uma especie de dicionario + matriz)
        seq_len é o tamanho da janela a ser utilizada na serie temporal
    '''
    def prepare_advertising_dataset(self, df_dados):
        qt_atributos = len(df_dados.columns)

        # Converter dataframe para matriz (lista com lista de cada registo)
        mat_dados = df_dados.as_matrix()
        tam_sequencia = self.sliding_window + 1

        # Numero de registos - tamanho da sequencia
        reg_tam_sequencia = len(mat_dados) - tam_sequencia

        res = []
        for i in range(reg_tam_sequencia):
            res.append(mat_dados[i: i + tam_sequencia])

        # Dá como resultado um np com uma lista de matrizes (janela deslizante ao longo da serie)
        res = np.array(res)

        # 67% passam a ser casos de treino
        qt_casos_treino = int(round(self.training_percentage * res.shape[0]))
        train = res[:qt_casos_treino, :]

        # Menos um registo pois o ultimo registo é o registo a seguir à janela
        X_train = train[:, :-1]
        X_test = res[qt_casos_treino:, :-1]

        # Para ir buscar o último atributo para a lista dos labels
        Y_train = train[:, -1][:,-1]
        Y_test = res[qt_casos_treino:, -1][:,-1]

        X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], qt_atributos))
        X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], qt_atributos))
        return [X_train, Y_train, X_test, Y_test]

    # Etapa 4 - Definir a topologia da rede (arquitectura do modelo) e compilar
    def build_model_before(self):
        model = Sequential()

        model.add(LSTM(128, input_shape = (self.sliding_window, 2), return_sequences = True))
        model.add(Dropout(0.2))
        model.add(LSTM(64, return_sequences = False))
        model.add(Dropout(0.2))

        model.add(Dense(16, activation="linear", kernel_initializer = "uniform"))
        model.add(Dense(1, activation="linear", kernel_initializer = "uniform"))

        model.compile(loss = 'mse', optimizer = 'adam', metrics = ['accuracy'])
        return model

    def build_model_after(self):
        model = Sequential()

        model.add(LSTM(256, input_shape = (self.sliding_window, 2), return_sequences = True))
        model.add(Dropout(0.2))
        model.add(LSTM(128, return_sequences = False))
        model.add(Dropout(0.2))

        model.add(Dense(64, activation="linear", kernel_initializer = "uniform"))
        model.add(Dense(32, activation="linear", kernel_initializer = "uniform"))
        model.add(Dense(16, activation="linear", kernel_initializer = "uniform"))
        model.add(Dense(1, activation="linear", kernel_initializer = "uniform"))

        model.compile(loss = 'mse', optimizer = 'adam', metrics = ['accuracy'])
        return model

    # Etapa 5 - Realizar plot dos resultados
    def print_model(self, model, filename):
        plot_model(model, to_file=filename, show_shapes=True, show_layer_names=True)

    def print_history_accuracy(self, history):
        plt.plot(history.history['acc'])
        plt.plot(history.history['val_acc'])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()

    def print_history_loss(self, history):
        plt.plot(history.history['loss'])
        plt.plot(history.history['val_loss'])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.show()

    # Imprime um grafico com os valores de teste e com as correspondentes tabela de previsões
    def print_series_prediction(self, real, predict):
        size = len(real)
        diff, ratio = [], []

        for i in range(size):
            diff.append(abs(real[i] - predict[i]))
            ratio.append((real[i]/predict[i]) - 1)
            print('Value: %.2f ---> Prevision: %.2f Diff: %.2f Ratio: %.2f' % (real[i], predict[i], diff[i], ratio[i]))

        plt.plot(real, color='blue', label='Real values')
        plt.plot(predict, color='red', label='Prediction')
        plt.plot(diff, color='green', label='Difference')
        plt.plot(ratio, color='yellow', label='Ratio')
        plt.legend(loc='upper left')
        plt.show()

    # ----- MAIN -----

    def predict_sequence_full(model, data):
        data_len = len(data)
        curr_frame = data[0]
        predicted = []
        for i in range(data_len):
            predicted.append(model.predict(curr_frame[newaxis, :, :])[0, 0])
            curr_frame = curr_frame[1:]
            curr_frame = np.insert(curr_frame, [self.window_size - 1], predicted[-1], axis=0)
        return predicted

    def LSTM_advertising(self):
        print('\n> [DATA SETTINGS]\n')
        print('     File: "%s"' % self.dataset_filename)
        print('     Training percentage: "%.2f%%"' % (self.training_percentage * 100))
        print('     Sliding window value: "%d"' % self.sliding_window)

        print('\n> [TEST HEADER]\n')
        # Visualize data
        self.visualize_advertising_dataset(self.dataset_filename)
        print('...')

        print('\n> Read', end='', flush=True)
        # Load data
        df = self.get_advertising_dataset(self.dataset_filename)

        print(' -> Prepare', end='', flush=True)
        # Prepare data
        X_train, Y_train, X_test, Y_test = self.prepare_advertising_dataset(df)

        print(', Done!')

        print('\n> [SHAPE]\n')
        print("     table: ", df.shape)
        print("     X_train: ", X_train.shape)
        print("     Y_train: ", Y_train.shape)
        print("     X_test: ", X_test.shape)
        print("     Y_test: ", Y_test.shape)

        print('\n> [NETWORK SETTINGS]\n')
        print('     Batch size: "%d"' % self.batch_size)
        print('     Epochs: "%d"' % self.epochs)

        print('\n> [BUILD AND TRAIN]\n')
        # Construir modelo
        model = self.build_model_after()

        # Treinar modelo
        history = model.fit(X_train, Y_train, batch_size=self.batch_size, epochs=self.epochs, validation_split=0.1, verbose=1)

        # Avaliar modelo
        print('\n> [EVALUATION]\n')

        trainScore = model.evaluate(X_train, Y_train, verbose=0)
        print('     Train Score: %.2f MSE (%.2f RMSE)' % (trainScore[0], math.sqrt(trainScore[0])))

        testScore = model.evaluate(X_test, Y_test, verbose=0)
        print('     Test Score: %.2f MSE (%.2f RMSE)' % (testScore[0], math.sqrt(testScore[0])))

        # Prever valores
        p = model.predict(X_test)

        # Para transformar uma matriz de uma coluna e n linhas em um np array de n elementos
        predict = np.squeeze(np.asarray(p))

        print('\n> [PRINTS]\n')
        print('> Save', end='', flush=True)
        # Guardar em ficheiro estrutura da rede neuronal
        self.print_model(model, "lstm_model.png")

        # Realizar plot da accuracy e loss
        print(' -> Accuracy', end='', flush=True)
        self.print_history_accuracy(history)
        print(' -> Loss', end='', flush=True)
        self.print_history_loss(history)

        print(' -> Prediction:\n')

        # Realizar plot da previsao
        self.print_series_prediction(Y_test, predict)
        print('\n> Done!')

if __name__ == '__main__':
    adver = ADVERTISING()
    adver.LSTM_advertising()
