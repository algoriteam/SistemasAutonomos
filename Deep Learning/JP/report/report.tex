\documentclass[conference]{IEEEtran}
\usepackage[portuguese]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{array,multirow,graphicx}

\lstset{
	tabsize=4,
}

\begin{document}

	\title{Previsão de lucros usando Redes Neuronais Recorrentes}

	\author{\IEEEauthorblockN{João Pedro Fontes}
	\IEEEauthorblockA{Universidade do
	Minho,\\ Braga, Portugal\\
	a71184@alunos.uminho.pt}}

	\maketitle

	\begin{abstract}
		Este documento pretende apresentar um estudo sobre redes neuronais recorrentes para previsão de lucros de vendas de um produto para emagrecimento através dos valores gastos em publicidade. Estes dados estão organizados numa série temporal de 36 meses, motivando assim a utilização das camadas recorrentes com memória que permitem atingir melhores resultados do que simples camadas \textit{Fully-Connected}. Com isto, o objetivo principal foi baixar uma métrica de erro, o RMSE, na previsão dos valores dos últimos 12 meses. Este estudo passou pelas diversas fases, desde a preparação do \textit{dataset} até à definição da arquitetura da rede e obtenção de resultados, onde se pode verificar a utilidade das camadas LSTM.
	\end{abstract}

	\section{Introdução}
		As redes neuronais recorrentes são redes neuronais nas quais existem certos nodos cujas saídas podem influenciar as suas entradas, criando uma espécie de ciclo. Dentro destas redes, encontramos as redes LSTM - \textit{Long Short-Term Memory}. Estas camadas são muito úteis no caso de análise de dados que apresentem séries temporais, permitindo usar uma janela deslizante nos dados que influencia o treino da rede. As camadas LSTM apresentam memória, açgo que não acontece com as camadas vulgares de uma rede neuronal, permitindo assim aprender os detalhes das séries temporais e atingir melhores resultados na previsão de valores.

		\medskip
		No caso do \textit{dataset} que foi analisado, este continha dados acerca das vendas de um produto para emagrecimento e dos gastos com a publicidade com este mesmo produto. Estes dados estavam representados por mês e extendem-se através de 36 meses. Para analisar este \textit{dataset}, a linguagem \textit{Python} e a biblioteca \textit{Keras} tornaram-se muito úteis, uma vez que permitiram ter uma API de alto nível, a trabalhar como \textit{frontend} para duas das APIs de \textit{Deep Learning} mais usadas no mercado, o \textit{Theano} e o \textit{TensorFlow}. Para este estudo, foi usado com \textit{backend} a biblioteca \textit{Theano} por apresentar resultados mais satisfatórios em relação à biblioteca \textit{TensorFlow}.

		\medskip
		Sendo assim, o objetivo do trabalho foi treinar um modelo, usando redes LSTM, que permitisse prever os valores das vendas através dos gastos com a publicidade. Para isso, deveriam ser usados os primeiros 24 meses (2 anos) para treino e os últimos 12 meses (1 ano) com vista a diminuir o valor do erro do modelo, calculado a partir da métrica RMSE - \textit{Root-Mean-Square Error}.

		\medskip
		Neste relatório é apresentado primeiro como foi feita a leitura e preparação do \textit{dataset} para análise, seguida de uma explicação dos passos de otimização da arquitetura da rede LSTM, apresentando posteriormente os resultados conseguidos. Por fim, são retiradas breves conclusões acerca deste trabalho.

	\section{Leitura e Preparação do \textit{Dataset}}
		% read dataset
		Para iniciar o nosso processo de análise do \textit{dataset} usando a linguagem \textit{Python} e a biblioteca \textit{Keras}, temos inicialmente de ler o ficheiro CSV que contém os dados para análise. Isto é conseguido graças às funções \textit{read\_csv} e \textit{DataFrame} da biblioteca \textit{pandas}. Com isto, obtemos uma tabela (\textit{data frame}) em memória com os dados presentes no ficheiro CSV.

		\medskip
		% dataset preparation
		Após a leitura do \textit{dataset}, é necessário prepará-lo para a análise. Uma vez que os dados são muito simples e como apenas nos interessa os valores gastos com publicidade e os ganhos com vendas, vamos descartar a coluna \textit{Month} usando a função \textit{drop} da biblioteca \textit{pandas}. As operações referidas foram executadas com recurso ao seguinte método \textit{Python}:

		\begin{lstlisting}[language=Python]
def get_data(adv_name,
			 normalized = 0,
			 file_name = None):
	cols = ["Month",
			"Advertising",
			"Sales"]
	adv = pd.read_csv(file_name,
					  header = 0,
					  names = cols)
	df = pd.DataFrame(adv)
	df.drop(df.columns[0],
			axis = 1,
			inplace = True)
	return df
		\end{lstlisting}

		\medskip
		% train and test frames build
		Tendo o \textit{dataset} preparado, podemos agora dividi-lo em casos de treino e teste, sendo que os registos dos dois primeiros anos (24 registos) são usados para treino e os restantes casos (12 registos) são usados para teste. Isto foi conseguido recorrendo ao seguinte código \textit{Python}

		\begin{lstlisting}[language=Python]
def load_data(df_dados,
			  janela):
	qt_atributos =
		len(df_dados.columns)
	mat_dados =
		df_dados.as_matrix()
	tam_sequencia = janela + 1
	res = []
	for i in
		range(len(mat_dados) -
			  tam_sequencia):
		res.append(mat_dados[i: i +
				   tam_sequencia])
	res = np.array(res)
	qt_casos_treino = int(round(0.67 *
						  res.shape[0]))
	train = res[:qt_casos_treino, :]
	x_train = train[:, :-1]
	y_train = train[:, -1][:,-1]
	x_test = res[qt_casos_treino:, :-1]
	y_test = res[qt_casos_treino:, -1]
		[:,-1]
	x_train = np.reshape(x_train,
		(x_train.shape[0],
		x_train.shape[1],
		qt_atributos))
	x_test = np.reshape(x_test,
		(x_test.shape[0],
		x_test.shape[1],
		qt_atributos))
	return [x_train, y_train,
			x_test, y_test]
		\end{lstlisting}

	\section{Arquitetura da Rede Neuronal}
		% architecture build
		Após a preparação dos dados a analisar e da construção dos \textit{frames} de treino e teste, foi necessário definir a arquitetura da rede de modo a minimizar a métrica de erro definida anteriormente, o RMSE (root-mean-square error). Sendo assim, inicialmente, foi definida a arquitetura para a rede neuronal recorrente dada como exemplo nas aulas. Esta arquitetura usa camadas \textit{LSTM} (duas com 128 e 64 nós, respetivamente) e camadas \textit{Fully-Connected} (duas com 16 e 1 nodos, respetivamente), sendo essa arquitetura apresentada na figura \ref{initial-net}. Adicionalmente, foi também adicionada uma camada de \textit{Dropout} entre as camadas \textit{LSTM} para reduzir a possibilidade de \textit{overfitting}.

		\medskip
		Os dados dos hiper-parametros iniciais são os seguintes:

		\begin{itemize}
			\item Número de \textit{epochs} de 500 (parâmetro epochs);
			\item Tamanho de \textit{batch} de 512 (parâmetro batch\_size);
			\item Tamanho de janela de 22 (parâmetro janela);
			\item Percentagem de dados para validação de 10\% (parâmetro validation\_split).
		\end{itemize}

		\medskip
		% hyperparameter optimization
		Sendo que o valor de RMSE ainda se encontra muito alto, foi necessário otimizar os hiper-parametros da rede de modo a baixar o valor do erro. Sendo assim, e após se verificar os gráficos de evolução do erro, verificou-se que o erro estagnou, pelo que um aumento no número de épocas de treino não iria ajudar. Começou-se por mudar os valores do \textit{batch\_size}, mantendo todos os outros hiper-parâmetros intocáveis. Novamente o erro estagnou e não se obteve resultados melhores. A grande baixa no erro foi causada quando se passou a percentagem de dados para validação para 33\%. Com isto, foi possível obter um erro mais baixo e chegar ao valor ótimo de um dos parâmetros. Após isto, foi decidida uma mudança na arquitetura da rede, passando a englobar mais camadas \textit{Fully-Connected}. Após alguns testes, a arquitetura que obteve melhores resultados apresentava 4 camadas \textit{Fully-Connected} com 32, 8, 2 e 1 nodos, respetivamente. Outra decisão importante foi a redução do valor do tamanho de janela, que passou para apenas 2, produzindo assim resultados muito melhores. Após a otimização de todos os hiper-parâmetros, a arquitetura final da rede é a seguinte:

		\begin{itemize}
			\item O número de \textit{epochs} de treino passou para 400;
			\item O tamanho de \textit{batch} passou para 8;
			\item O tamanho de janela passou para 2;
			\item A percentagem de dados para validação passou para 33\%
			\item A arquitetura da rede passou a englobar quatro camadas \textit{Fully-Connected} (com 32, 8, 2 e 1 nodos, respetivamente) em vez das duas iniciais.
		\end{itemize}

		\medskip
		A arquitetura final da rede pode ser vista na figura \ref{final-net}.

	\section{Resultados}
		Após alguns testes, a arquitetura inicial conseguiu um \textit{score} de \textbf{MSE = 545.79} e \textbf{RMSE = 23.36}. Este \textit{score} já era baixo mas ainda podia baixar, daí a motivação para procurar um arquitetura que fizesse baixar os valores de RMSE.

		Sendo assim, e após o teste de várias configurações e valores dos hiper-parâmetros, chegou-se à arquitetura final referida anteriormente. Esta arquitetura conseguiu um \textit{score} de \textbf{MSE = 265.15} e \textbf{RMSE = 16.28}, sendo que foi o valor mais baixo que foi possível obter com os testes feitos com este \textit{dataset}.

	\section{Conclusão}
		Com este trabalho foi possível observar como a utilização de camadas recorrentes são úteis na previsão de valores que sejam ordenados numa série temporal, como era o caso do \textit{dataset} utilizado. Estas camadas permitiram obter melhores previsões em relação a simples camadas \textit{Fully-Connected}, que nos iriam dar valores de RMSE mais altos, estragando assim as previsões. Foi também observado que, neste caso, a normalização de dados estragava a previsão de valores, uma vez que estragava a capacidade de generalização da rede.

	\begin{thebibliography}{9}
		\bibitem{alves2017}
			Apontamentos das aulas de Sistemas Autónomos, Victor Alves, Universidade do Minho, 2017
	\end{thebibliography}
	\bibliographystyle{ieeetran}

	\cleardoublepage
	\onecolumn
	\appendix
		\begin{figure}[ht]
			\centering
			\includegraphics[width=0.5\textwidth]{images/initial.png}
			\caption{Arquitetura inicial da rede}
			\label{initial-net}
		\end{figure}

		\begin{figure}[ht]
			\centering
			\includegraphics[width=0.5\textwidth]{images/final.png}
			\caption{Arquitetura final da rede}
			\label{final-net}
		\end{figure}

		\begin{figure}[ht]
			\centering
			\includegraphics[width=0.65\textwidth]{images/error-initial}
			\caption{Evolução do erro da arquitetura inicial}
			\label{error-initial}
		\end{figure}

		\begin{figure}[ht]
			\centering
			\includegraphics[width=0.65\textwidth]{images/error-final}
			\caption{Evolução do erro da arquitetura final}
			\label{error-final}
		\end{figure}
\end{document}
